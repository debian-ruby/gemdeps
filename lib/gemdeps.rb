require "open-uri"
require "yaml"
require File.expand_path(__FILE__)+"/../debian"

class Gemfile
  attr_accessor :name, :fetched_deps, :deps, :bdeps, :color, :debver, :gemver, :suite

 @@exceptions = {
     "bundler" => "bundler",
     "capistrano" => "capistrano",
     "cucumber" => "cucumber",
     "rake" => "rake",
     "rdoc" => "rdoc",
     "unicorn" => "unicorn",
     "rubyzip" => "ruby-zip",
     "rubyntlm" => "ruby-ntlm",
     "thin" => "thin",
     "racc" => "racc",
     "asciidoctor" => "asciidoctor",
     "pry" => "pry",
     "rake" => "rake",
     "messagebus_ruby_api" => "ruby-messagebus-api",
   }

  def initialize(name)
    @name = name
    @fetched_deps = false
    @deps = []
    @bdeps = []
    @color = nil
    @debver = ""
    @suite = ""
  end

  def fetch_deps!
    puts "I: fetch deps for #{name}. #{@fetched_deps}" if ENV["VERBOSE"]
    if !@fetched_deps
      begin
        yaml = open("https://rubygems.org/api/v1/gems/#{@name}.yaml") { |f| f.read }
        geminfo = YAML.load(yaml)
        @gemver = geminfo["version"]

        geminfo["dependencies"]["runtime"].each do |k|
          @deps << k["name"]
        end
        geminfo["dependencies"]["development"].each do |k|
          @bdeps << k["name"]
        end
        @fetched_deps = true
      rescue OpenURI::HTTPError
        $stderr.puts "E: Cannot fetch deps of #{@name} gem"
      end
      puts "I: deps fetched for #{name}. #{@fetched_deps}" if ENV["VERBOSE"]
    end
  end

  def register(gem_list)
    fetch_deps!
    if !gem_list.has_key? name.to_sym
      gem_list[name.to_sym] = self
    end

  end

  def register_deps(gem_list)
    register(gem_list)
    deps.each do |dep|
      gem_dep = gem_list[dep.to_sym] || Gemfile.new(dep)
      gem_dep.register_deps(gem_list)
    end
  end

  def debian_name
    if @@exceptions.has_key? @name
      return @@exceptions[@name]
    else
      # regexp from gem2deb
      return "ruby-" + @name.gsub(/_/,'-').gsub(/^ruby[-_]|[-_]ruby$/, '').downcase
    end
  end

  def packaged_in_debian?
    # search in debian repository
    #return `aptitude search ^#{debian_name}$` != ""
    #return `apt-cache search ^#{debian_name}$` != ""
    puts debian_name
    latest_version=`rmadison -u debian -s sid,unstable,experimental -a amd64,all #{debian_name}`.lines.last || " | | | "
    @debver,@suite = latest_version.split("|")[1,2]
    return @debver !=""
  end

  def is_in_list? pkglist
    return pkglist.include? debian_name
  end

  def wnpp_status
    result = `wnpp-check #{debian_name}`
    if result =~ /^\((ITP|RFP) - \#([0-9]*)\).* #{debian_name}$/
      return $~[1]
    else
      return nil
    end
  end

  def set_color!
    begin
      if is_in_list? Debian.show_packages_in_new
        @color="yellow"
      elsif (wnpp=wnpp_status)
        if wnpp == "ITP"
          @color="orange"
        else
          @color="purple"
        end
      elsif packaged_in_debian?
        if @suite =~ /sid|unstable/
          @color="green"
        elsif @suite =~ /experimental/
          @color="blue"
        end
      end
    end unless @color
  end
end

class Gemlist < Hash
  def to_dot(options= {:colors => true, :todo => true, :bdeps => false})
    s=""
    s << "digraph G {\n"
    each do |name, gemfile|
      if options[:colors]
        gemfile.set_color!
        s << name.to_s.quoted
        s << '[label="' << name.to_s
        s << '\n' << "gem: " << (gemfile.gemver || "")
        if gemfile.debver != ""
          s << '\n' << "deb: " << gemfile.debver << "\nsuite: " << gemfile.suite
        end
        s << '"'
        if gemfile.color and (gemfile.color != "green" or not options[:todo])
          s << ", color=" 
          s << gemfile.color.quoted
        end
        s << "];\n"
      end
    end

    each do |name, gemfile|
      if gemfile.color != "green" or not options[:todo]
        gemfile.deps.each do |dep|
          s << name.to_s.quoted << " -> " << dep.to_s.quoted << ";\n"
        end
        if options[:bdeps]
          gemfile.bdeps.each do |bdep|
            s << name.to_s.quoted << " -> " << bdep.to_s.quoted << " [style=dotted];\n"
          end
        end
      end
    end
    s << "}\n"
    return s
  end

  def to_cache(filename="gemlist"+`date +%Y%m%d`.chomp)
    File.open(filename, 'w') do |f|
      f << to_yaml
    end
  end

  def Gemlist.from_cache(filename="gemlist"+`date +%Y%m%d`.chomp)
    yaml = open(filename) { |f| f.read }
    return YAML.load(yaml)
  end

end

class String
  def quoted
    s = String.new
    s << "\"" << self << "\""
    return s
  end
end
